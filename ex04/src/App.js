import './App.css';
import ToDoList from './ToDoList';
import {useState} from 'react'

const InitialTodos = [
  { description: 'Finish lecture', isComplete: true },
  { description: 'Do homework', isComplete: false },
  { description: 'Sleep', isComplete: true }
];

export default function App() {
  const [todos,setTodos] = useState(InitialTodos);
  
  return <ToDoList items={todos} 
  onTodoStatusChanged={(index, status) => {
    var newTodo = [...todos];
    newTodo[index].isComplete = status;
    setTodos(newTodo);
  }} />
}





