import React from 'react'

export default function AddNewItem(props) {
    function add(){
        const txtInput = document.getElementById("description-text");
        console.log(txtInput);
        props.onAddButtonClicked(txtInput.value);
    }

    return (
        <div>
            <input id="description-text" type="text"/>&nbsp;
            <button onClick={add}>add new item</button>
        </div>
    )
}