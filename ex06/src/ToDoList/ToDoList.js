import React from 'react'
import './ToDoList.css'

export default function ToDoList(props) {

    //handlers
    const check = (e,index) => props.onTodoStatusChanged(index, e.target.checked)
    const del = index => props.onDeleteButtonClicked(index)


    return <div>
        <h1>My Todos</h1>
        {props.items && props.items.length > 0 ?
            props.items.map((item, index) =>
                <div className="list" key={index}>
                    <label
                        className={item.isComplete ? "item-complete" : ""}>
                        <input type="checkbox"
                               onChange={(e) => check(e,index)}
                               checked={item.isComplete}/>
                        {item.description}
                        {item.isComplete && <span>(Done!)</span>}
                    </label>
                    <button onClick={() => del(index)}>delete</button>
                </div>)
            : <p>There are no to-do items!</p>}
    </div>


}