import React from "react"
import ToDoList from './ToDoList'

export default function App(){

    return <ToDoList items={["Finish lecture", "Do homework", "Sleep"]} />

}