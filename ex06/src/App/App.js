import './App.css';
import ToDoList from '../ToDoList/ToDoList';
import {useState} from 'react'
import AddNewItem from "../AddNewItem/AddNewItem";

const InitialTodos = [
    {description: 'Finish lecture', isComplete: true},
    {description: 'Do homework', isComplete: false},
    {description: 'Sleep', isComplete: true}
];

export default function App() {
    const [todos, setTodos] = useState(InitialTodos);

    //handlers
    function checkItem(index, status){
        const newTodos = [...todos];
        newTodos[index].isComplete = status;
        setTodos(newTodos);
    }

    function deleteItem(index){
        const newTodos =  [...todos];
        newTodos.splice(index,1);
        setTodos(newTodos);
    }

    function addItem(des){
        const newTodos = [...todos];
        newTodos.push({
            'description': des,
            isComplete: false
        })
        setTodos(newTodos)
    }

    return (
        <div>
            <ToDoList items={todos}
                      onTodoStatusChanged={checkItem}
                      onDeleteButtonClicked={deleteItem} />
            <AddNewItem onAddButtonClicked={addItem} />
        </div>)
}





