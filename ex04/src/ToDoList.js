import React from 'react'
import './ToDoList.css'

export default function ToDoList(props){

    function change(e){
        props.onTodoStatusChanged(e.target.value, e.target.checked)
    }

    return <div>
        <h1>My Todos</h1> 
        {props.items.length !== 0 ? 
        props.items.map((item,index) => <div className="list" key={index}><label className={item.isComplete ? "item-complete" : ""}><input value={index} type="checkbox" 
        onChange={change} 
        checked={item.isComplete} />
        {item.description}
        {item.isComplete && <span>(Done!)</span>}</label></div>)
        : <p>There are no to-do items!</p>}
    </div>


}