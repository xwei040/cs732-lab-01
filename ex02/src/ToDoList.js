import React from 'react'

function ToDoList(props) {
    return props.items && props.items.length !== 0 ? <ul>
          {props.items.map((item,index) => <li key={index}>{item}</li>)}
          </ul> : <p>There is no item</p>
  
}

export default ToDoList;

